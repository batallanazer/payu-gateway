<?php
include 'config.php';
include 'connect.php';
$action = '';

$posted = array();
if(!empty($_POST)) {
    //print_r($_POST);
  foreach($_POST as $key => $value) {    
    $posted[$key] = $value; 
	
  }
}

$formError = 0;
$txnid = $_GET["invoiceNo"];
$sql = "SELECT * FROM invoice WHERE invoiceno = '$txnid'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$currency = $row["currency"];
$amount = $row["total"];
$conn->close();
// if(empty($posted['txnid'])) {
//   // Generate random transaction id
//   $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
// } else {
//   $txnid = $posted['txnid'];
// }
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if(empty($posted['hash']) && sizeof($posted) > 0) {
  if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
		  || empty($posted['service_provider'])
  ) {
    $formError = 1;
  } else {
    //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
	$hashVarsSeq = explode('|', $hashSequence);
    $hash_string = '';	
	foreach($hashVarsSeq as $hash_var) {
      $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
      $hash_string .= '|';
    }

    $hash_string .= $SALT;


    $hash = strtolower(hash('sha512', $hash_string));
    $action = $PAYU_BASE_URL . '/_payment';
  }
} elseif(!empty($posted['hash'])) {
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}
?>
<html>
  <head>
  <style>
        .hide{
            display:none;
        }
  </style>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
      if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
  </script>
  </head>
  <body onload="submitPayuForm()" class="bg-light">
    <br/>
   

  
    <div class="container">

      <div class="row">
        
         <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill"><?php echo $currency.' '.$amount?></span>
          </h4>
          <ul class="list-group mb-3">
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">Product name</h6>
                <small class="text-muted">Brief description</small>
              </div>
              <span class="text-muted">$12</span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">Second product</h6>
                <small class="text-muted">Brief description</small>
              </div>
              <span class="text-muted">$8</span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0">Third item</h6>
                <small class="text-muted">Brief description</small>
              </div>
              <span class="text-muted">$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between bg-light">
              <div class="text-success">
                <h6 class="my-0">Promo code</h6>
                <small>EXAMPLECODE</small>
              </div>
              <span class="text-success">-$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (USD)</span>
              <strong>$20</strong>
            </li>
          </ul>
        </div> 
        <div class="col-md-8 order-md-1">
            <?php if($formError) { ?>
                <span style="color:red">Please fill all mandatory fields.</span>
                <br/>
            <?php } ?>
         
          <h4 class="mb-3">Billing address</h4>
          <form class="needs-validation" action="<?php echo $action; ?>" method="post" name="payuForm">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
		        <input name="firstname" id="firstname" value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" class="form-control"/>
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input  name="lastname" id="lastname" value="<?php echo (empty($posted['lastname'])) ? '' : $posted['lastname']; ?>" class="form-control"/>
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label for="email">Email</label>
              <input name="email" id="email" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" class="form-control"/>
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>
            <div class="mb-3">
              <label for="phone">Phone</label>
              <input name="phone" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" class="form-control"/>
              <div class="invalid-feedback">
                Please enter a valid phone number for shipping updates.
              </div>
            </div>
            <div class="mb-3">
              <label for="address1">Address 1 <span class="text-muted">(Optional)</span></label>
              <input  name="address1" value="<?php echo (empty($posted['address1'])) ? '' : $posted['address1']; ?>" class="form-control"/>
            </div>

            <div class="mb-3">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input  name="address2" value="<?php echo (empty($posted['address2'])) ? '' : $posted['address2']; ?>" class="form-control"/>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">Country <span class="text-muted">(Optional)</span></label>
                <input  name="country" value="<?php echo (empty($posted['country'])) ? '' : $posted['country']; ?>" class="form-control"/>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">State <span class="text-muted">(Optional)</span></label>
              	<input  name="state" value="<?php echo (empty($posted['state'])) ? '' : $posted['state']; ?>" class="form-control"/>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">Zip <span class="text-muted">(Optional)</span></label>
                <input  name="zipcode" value="<?php echo (empty($posted['zipcode'])) ? '' : $posted['zipcode']; ?>" class="form-control"
              </div>
            </div>
            <hr class="mb-4">
            <!-- <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button> -->
            <input class="btn btn-primary btn-lg btn-block" type="submit" value="Continue to checkout" />
            <input type="hidden" name="key" value="<?php echo $MERCHANT_KEY; ?>" />
            <input type="hidden" name="hash" value="<?php echo $hash; ?>"/>
            <input type="hidden" name="txnid" value="<?php echo $txnid; ?>" />
            <input type="hidden" name="service_provider" value="payu_paisa"/>
            <input type="hidden" name="amount" value="<?php echo $amount; ?>" />
            <input type="hidden" name="productinfo" value="Arcadier" />
            <input type="hidden" name="surl" value="<?php echo $sUrl; ?>"/>
            <input type="hidden" name="furl" value="<?php echo $fUrl; ?>"/>
            <input type="hidden" name="udf1" value="<?php echo (empty($posted['udf1'])) ? '' : $posted['udf1']; ?>"/>
            <input type="hidden" name="udf2" value="<?php echo (empty($posted['udf2'])) ? '' : $posted['udf2']; ?>"/>
            <input type="hidden" name="udf3" value="<?php echo (empty($posted['udf3'])) ? '' : $posted['udf3']; ?>"/>
            <input type="hidden" name="udf4" value="<?php echo (empty($posted['udf4'])) ? '' : $posted['udf4']; ?>"/>
            <input type="hidden" name="udf5" value="<?php echo (empty($posted['udf5'])) ? '' : $posted['udf5']; ?>"/>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>


